document.addEventListener('DOMContentLoaded', function () {

    // Get all "navbar-burger" elements
    var $navbars = Array.prototype.slice.call(document.querySelectorAll('a.navbar-item[href^="#"]'), 0);

    // Check if there are any navbar burgers
    if ($navbars.length > 0) {
        
      // Add a click event on each of them
      $navbars.forEach(function ($el) {

        $el.addEventListener('click', function () {

          // Get the target from the "data-target" attribute
          var target = $el.dataset.target;
          var $target = document.getElementById(target);

          // Toggle the class on both the "navbar-burger" and the "navbar-menu"
          $navbars.map($a => {
            if($a.classList.contains('is-active')) {
                $a.classList.remove('is-active')
            }
          });
          $el.classList.toggle('is-active');

        });
      });
    }

    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach( el => {
        el.addEventListener('click', () => {

            // Get the target from the "data-target" attribute
            const target = el.dataset.target;
            const $target = document.getElementById(target);

            // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
            el.classList.toggle('is-active');
            $target.classList.toggle('is-active');

        });
        });
    }

  });

  const scroll = new SmoothScroll('a[href*="#"]', {
	speed: 1000
});

// hide navbar on scroll down,
// then recover navbar on scroll stop
let prevScrollpos = window.pageYOffset;
let currentScrollPos;

const delayedExec = function(after, fn) {
  let timer;
  
  return function(e) {
    e.preventDefault();
    currentScrollPos = window.pageYOffset;
    
    if (prevScrollpos > currentScrollPos) {
      document.querySelector(".navbar").style.top = "0";
    } else {
      document.querySelector(".navbar").style.top = "-63px";
    }
    prevScrollpos = currentScrollPos;

    timer && clearTimeout(timer);
    timer = setTimeout(fn, after);
  };
};

const scrollStopper = delayedExec(100, function() {
  // handle scroll down stop event
  document.querySelector(".navbar").style.top = "0";
  console.log('stopped it');
});

window.addEventListener('scroll', scrollStopper);

document.querySelectorAll('.year').forEach((year, index) => {
  year.innerHTML = new Date().getFullYear()
})